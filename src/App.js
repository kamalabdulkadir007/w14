import React, { useState } from 'react';
import './App.css';

function App() {
  const [text, setText] = useState("");

  let chat = [];

  function read() {
    const sentMessage = JSON.parse(localStorage.getItem("chat"));
    document.getElementById("showMessages").value = sentMessage + "\n";
  };

  function write() {
    chat.push(document.getElementById("chat").value);
    localStorage.setItem("chat", JSON.stringify(chat));
  };

  return (
    <div className="App">
      <div className="Chat">
        <input
          type="text"
          id="chat"
          className="Chat"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <button onClick={write} className="Chatsend">Send</button>
        <button onClick={read} className="Chatread">Show messages</button>
        <textarea className="Chat" id="showMessages"></textarea>
      </div>
      <hr></hr>
      <p>
        {text || `Tekstiä ei ole syötetty vielä.`}
      </p>

      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ornare massa eget egestas purus viverra accumsan. Sed lectus vestibulum mattis ullamcorper velit sed.
        Nullam vehicula ipsum a arcu cursus vitae congue mauris rhoncus. Senectus et netus et malesuada. Turpis nunc eget lorem dolor sed.
        Sem integer vitae justo eget magna fermentum iaculis eu. Diam quam nulla porttitor massa id neque aliquam.
        Id consectetur purus ut faucibus pulvinar. Nunc mi ipsum faucibus vitae. Massa ultricies mi quis hendrerit dolor magna eget.
        Lobortis feugiat vivamus at augue eget arcu dictum.
      </p>

      <p>
        Elementum eu facilisis sed odio. Elit at imperdiet dui accumsan.
        In nulla posuere sollicitudin aliquam ultrices sagittis.
        Sit amet nulla facilisi morbi tempus iaculis urna id.
        In fermentum et sollicitudin ac orci phasellus. Volutpat lacus laoreet non curabitur gravida arcu.
        Odio ut sem nulla pharetra diam. Nunc vel risus commodo viverra maecenas.
        Velit scelerisque in dictum non consectetur a. Massa sapien faucibus et molestie ac feugiat sed lectus.
        Diam in arcu cursus euismod quis.
      </p>

      <p>
        At erat pellentesque adipiscing commodo elit at. Eros in cursus turpis massa tincidunt dui ut.
        Et malesuada fames ac turpis egestas. Massa sed elementum tempus egestas sed sed risus pretium.
        Diam maecenas ultricies mi eget mauris pharetra et. Eget nulla facilisi etiam dignissim diam.
        Amet est placerat in egestas erat imperdiet sed euismod. Quis imperdiet massa tincidunt nunc pulvinar sapien et.
        Lacus luctus accumsan tortor posuere ac. Amet consectetur adipiscing elit duis tristique sollicitudin nibh.
        Gravida neque convallis a cras semper. Sagittis purus sit amet volutpat consequat. In arcu cursus euismod quis viverra nibh cras pulvinar.
      </p>

      <p>
        Felis eget nunc lobortis mattis. Massa tincidunt dui ut ornare lectus sit amet.
        Fermentum posuere urna nec tincidunt praesent semper feugiat nibh sed.
        Sed elementum tempus egestas sed sed risus pretium quam.
        Donec pretium vulputate sapien nec sagittis aliquam malesuada.
        Nulla aliquet porttitor lacus luctus. Sit amet consectetur adipiscing elit pellentesque.
        Elit at imperdiet dui accumsan sit amet. Cras sed felis eget velit aliquet sagittis id.
        Id diam vel quam elementum. Ullamcorper dignissim cras tincidunt lobortis feugiat.
        Blandit libero volutpat sed cras ornare arcu dui vivamus arcu. Tincidunt arcu non sodales neque sodales.
        Aliquam ut porttitor leo a.
      </p>

      <p>
        Ligula ullamcorper malesuada proin libero nunc consequat interdum. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl.
        Lacinia at quis risus sed vulputate. Dignissim suspendisse in est ante in nibh. Consectetur lorem donec massa sapien faucibus et molestie ac.
        Turpis egestas pretium aenean pharetra magna ac placerat vestibulum lectus. Enim tortor at auctor urna nunc id cursus.
        Commodo viverra maecenas accumsan lacus. Mauris vitae ultricies leo integer malesuada nunc vel.
        Mi ipsum faucibus vitae aliquet. Euismod nisi porta lorem mollis aliquam ut porttitor leo. Non tellus orci ac auctor augue mauris augue.
        Consectetur adipiscing elit ut aliquam purus sit amet. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis.
      </p>
    </div>
  )
}

export default App;
